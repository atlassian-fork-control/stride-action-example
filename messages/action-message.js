//Data structure for sending a dynamic action as a message to open dialog modal
// We're defining a link that has an action that is targeting a dialog that was defined in the app descriptor.
//https://developer.atlassian.com/cloud/stride/apis/document/marks/action/
let actionMessage = {
  version: 1,
  type: 'doc',
  content: [
    {
      type: 'paragraph',
      content: [
        {
          type: 'text',
          text: 'This is an action from inside a message to '
        },
        {
          type: 'text',
          text: 'Open a Dialog',
          marks: [
            {
              type: 'action',
              attrs: {
                title: 'view dialog',
                target: {
                  key: 'app-dialog'
                },
                parameters: {
                  param1: 'This is a parameter from from a text link in a message'
                }
              }
            }
          ]
        }
      ]
    }
  ]
}

module.exports = {
  actionMessage
}
